`timescale 1ns/100ps

module adder_tb;

reg [5:0] a;
reg [5:0] b;
wire [5:0] sum;

binary_adder adder_0 ( .a(a), .b(b), .sum(sum));

initial begin
end

initial begin
	a = 6'b000101; b = 6'b000011;       // sum should be  6'b001000
	# 100 $display(sum);
	# 100 a = 6'b000101; b = 6'b000111; // sum should be  6'b001100
	# 100 $display(sum);
	# 100 a = 6'b001001; b = 6'b000010; // sum should be  6'b001011
	# 100 $display(sum);
	# 100 a = 6'b011001; b = 6'b100010; // sum should be  6'b111011
	# 100 $display(sum);
	# 100 a = 6'b000000; b = 6'b000000; // sum should be  6'b000000
	# 100 $display(sum);
#100 a = 6'b000100; b = 6'b101100; // 48
#100 $display(sum);
#100 a = 6'b000011; b = 6'b110100; // 55
#100 $display(sum);
#100 a = 6'b001000; b = 6'b110001; // 57
#100 $display(sum);
#100 a = 6'b000110; b = 6'b101000; // 46
#100 $display(sum);
#100 a = 6'b001001; b = 6'b011001; // 34
#100 $display(sum);
#100 a = 6'b010000; b = 6'b001101; // 29
#100 $display(sum);
#100 a = 6'b001000; b = 6'b100010; // 42
#100 $display(sum);
#100 a = 6'b011000; b = 6'b011111; // 55
#100 $display(sum);
#100 a = 6'b000011; b = 6'b001011; // 14
#100 $display(sum);
#100 a = 6'b010110; b = 6'b100110; // 60
#100 $display(sum);
#100 a = 6'b001011; b = 6'b000010; // 13
#100 $display(sum);
#100 a = 6'b001101; b = 6'b011101; // 42
#100 $display(sum);
#100 a = 6'b011000; b = 6'b010011; // 43
#100 $display(sum);
#100 a = 6'b011000; b = 6'b100001; // 57
#100 $display(sum);
#100 a = 6'b100111; b = 6'b000110; // 45
#100 $display(sum);
#100 a = 6'b001010; b = 6'b110101; // 63
#100 $display(sum);
#100 a = 6'b000100; b = 6'b010110; // 26
#100 $display(sum);
#100 a = 6'b011001; b = 6'b011011; // 52
#100 $display(sum);
#100 a = 6'b001100; b = 6'b000011; // 15
#100 $display(sum);
#100 a = 6'b010110; b = 6'b101000; // 62
#100 $display(sum);
#100 a = 6'b000101; b = 6'b000000; // 5
#100 $display(sum);
#100 a = 6'b001100; b = 6'b000111; // 19
#100 $display(sum);
#100 a = 6'b001011; b = 6'b110001; // 60
#100 $display(sum);
#100 a = 6'b010011; b = 6'b011000; // 43
#100 $display(sum);
#100 a = 6'b001000; b = 6'b011011; // 35
#100 $display(sum);
#100 a = 6'b000101; b = 6'b011001; // 30
#100 $display(sum);
#100 a = 6'b011111; b = 6'b010101; // 52
#100 $display(sum);
#100 a = 6'b010110; b = 6'b001100; // 34
#100 $display(sum);
#100 a = 6'b011101; b = 6'b100001; // 62
#100 $display(sum);
#100 a = 6'b010110; b = 6'b010110; // 44
#100 $display(sum);
#100 a = 6'b101000; b = 6'b001111; // 55
#100 $display(sum);
#100 a = 6'b010001; b = 6'b000101; // 22
#100 $display(sum);
#100 a = 6'b100001; b = 6'b011000; // 57
#100 $display(sum);
#100 a = 6'b011000; b = 6'b011011; // 51
#100 $display(sum);
#100 a = 6'b011101; b = 6'b000100; // 33
#100 $display(sum);
#100 a = 6'b011101; b = 6'b011000; // 53
#100 $display(sum);
#100 a = 6'b010111; b = 6'b011101; // 52
#100 $display(sum);
#100 a = 6'b001001; b = 6'b000001; // 10
#100 $display(sum);
#100 a = 6'b011000; b = 6'b100000; // 56
#100 $display(sum);
#100 a = 6'b000000; b = 6'b010111; // 23
#100 $display(sum);
#100 a = 6'b101100; b = 6'b010001; // 61
#100 $display(sum);
#100 a = 6'b000101; b = 6'b101100; // 49
#100 $display(sum);
#100 a = 6'b101100; b = 6'b000011; // 47
#100 $display(sum);
#100 a = 6'b010010; b = 6'b001101; // 31
#100 $display(sum);
#100 a = 6'b000001; b = 6'b001110; // 15
#100 $display(sum);
#100 a = 6'b000000; b = 6'b100111; // 39
#100 $display(sum);
#100 a = 6'b010001; b = 6'b100100; // 53
#100 $display(sum);
#100 a = 6'b000111; b = 6'b001111; // 22
#100 $display(sum);
#100 a = 6'b111001; b = 6'b000100; // 61
#100 $display(sum);
#100 a = 6'b101010; b = 6'b001001; // 51
#100 $display(sum);
#100 a = 6'b000111; b = 6'b100011; // 42
#100 $display(sum);
#100 a = 6'b000000; b = 6'b001000; // 8
#100 $display(sum);
#100 a = 6'b001001; b = 6'b100101; // 46
#100 $display(sum);
#100 a = 6'b010001; b = 6'b100111; // 56
#100 $display(sum);
#100 a = 6'b001011; b = 6'b000101; // 16
#100 $display(sum);
#100 a = 6'b011011; b = 6'b011100; // 55
#100 $display(sum);
#100 a = 6'b000110; b = 6'b001001; // 15
#100 $display(sum);
#100 a = 6'b001101; b = 6'b000101; // 18
#100 $display(sum);
#100 a = 6'b001101; b = 6'b001010; // 23
#100 $display(sum);
#100 a = 6'b000110; b = 6'b001001; // 15
#100 $display(sum);
#100 a = 6'b000100; b = 6'b001001; // 13
#100 $display(sum);
#100 a = 6'b100010; b = 6'b000011; // 37
#100 $display(sum);
#100 a = 6'b100000; b = 6'b011111; // 63
#100 $display(sum);
#100 a = 6'b001100; b = 6'b011110; // 42
#100 $display(sum);
#100 a = 6'b001101; b = 6'b011011; // 40
#100 $display(sum);
#100 a = 6'b010000; b = 6'b101000; // 56
#100 $display(sum);
#100 a = 6'b101010; b = 6'b000011; // 45
#100 $display(sum);
#100 a = 6'b010110; b = 6'b011100; // 50
#100 $display(sum);
#100 a = 6'b000011; b = 6'b110011; // 54
#100 $display(sum);
#100 a = 6'b001101; b = 6'b100001; // 46
#100 $display(sum);
#100 a = 6'b010111; b = 6'b001101; // 36
#100 $display(sum);
#100 a = 6'b101010; b = 6'b001001; // 51
#100 $display(sum);
#100 a = 6'b011001; b = 6'b001101; // 38
#100 $display(sum);
#100 a = 6'b010101; b = 6'b001000; // 29
#100 $display(sum);
#100 a = 6'b010010; b = 6'b010100; // 38
#100 $display(sum);
#100 a = 6'b000100; b = 6'b000111; // 11
#100 $display(sum);
#100 a = 6'b001010; b = 6'b011101; // 39
#100 $display(sum);
#100 a = 6'b100000; b = 6'b010100; // 52
#100 $display(sum);
#100 a = 6'b000000; b = 6'b100111; // 39
#100 $display(sum);
#100 a = 6'b000000; b = 6'b101011; // 43
#100 $display(sum);
#100 a = 6'b011110; b = 6'b010000; // 46
#100 $display(sum);
#100 a = 6'b011111; b = 6'b011110; // 61
#100 $display(sum);
#100 a = 6'b010101; b = 6'b011110; // 51
#100 $display(sum);
#100 a = 6'b010100; b = 6'b000011; // 23
#100 $display(sum);
#100 a = 6'b001110; b = 6'b000010; // 16
#100 $display(sum);
#100 a = 6'b100100; b = 6'b011011; // 63
#100 $display(sum);
#100 a = 6'b100000; b = 6'b000000; // 32
#100 $display(sum);
#100 a = 6'b000011; b = 6'b101100; // 47
#100 $display(sum);
#100 a = 6'b111101; b = 6'b000010; // 63
#100 $display(sum);
#100 a = 6'b100101; b = 6'b000010; // 39
#100 $display(sum);
#100 a = 6'b000110; b = 6'b011111; // 37
#100 $display(sum);
#100 a = 6'b000111; b = 6'b110010; // 57
#100 $display(sum);
#100 a = 6'b011000; b = 6'b100110; // 62
#100 $display(sum);
#100 a = 6'b001000; b = 6'b101000; // 48
#100 $display(sum);
#100 a = 6'b001111; b = 6'b100100; // 51
#100 $display(sum);
	$finish;
end

endmodule
