for filename in ../hw* ;do
echo $filename
rm binary_adder.v full_adder.v binary_adder.vvp
iverilog -o binary_adder.vvp binary_adder_tb.v $filename/binary_adder.v $filename/full_adder.v
id="${filename##*/}"
echo -n "$id " >> grade2.txt
vvp binary_adder.vvp | python3 ../grade.py >> grade2.txt
echo "">> grade2.txt
done
